﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoPlayer_v2.View
{
	public interface IView
	{
		System.Windows.Forms.DialogResult ShowForm();
		void CloseForm();
	}
}
