﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoPlayer_v2.View
{
	interface IPlaylistForm : IView
	{
		string Password { get; }
		string PasswordConfirm { get; }
		string PlaylistName { get; }
		bool playlistValid { get; set; }
		bool isPrivate { get; }

		event EventHandler SaveBtnClick;
	}
}
