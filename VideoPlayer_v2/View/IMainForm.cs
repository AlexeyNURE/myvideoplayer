﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoPlayer_v2.View
{
	interface IMainForm : IView
	{
		int AudioSliderValue { get; set; }

		int VideoSliderValue { get; set; }

		ComboBox GetComboBox { get; }

		void changePlayImage( Bitmap _bitmap );

		void changeSoundImage( Bitmap _bitmap);

		Label nameLabel { get; }

		AxWMPLib.AxWindowsMediaPlayer MediaPlayer { get; set; }

		void setLeftBoundText(string _text);

		void setRightBoundText(string _text);

		Size VideoPanelSize { get; }

		ListBox playlistBox { get; }

		event EventHandler LoadMainForm;
		event EventHandler ClosedForm;
		event EventHandler BrowseClick;
		event EventHandler PlayClick;
		event EventHandler VideoSlide;
		event EventHandler AudioSlide;
		event EventHandler SoundClick;
		event EventHandler VideoDoubleClick;
		event EventHandler TimerTick;
		event EventHandler StopClick;
		event EventHandler EndClick;
		event EventHandler FullScreenClick;
		event EventHandler EscapeKey;
		event EventHandler LeftKey;
		event EventHandler RightKey;
		event EventHandler SpaceKey;
		event EventHandler UpKey;
		event EventHandler DownKey;
		event EventHandler AddPlaylistClick;
		event EventHandler ChoosePlaylist;
		event EventHandler AddVideoClick;
		event EventHandler PathDoubleClick;
		event EventHandler PrevButtonClick;
		event EventHandler NextButtonClick;
		event EventHandler ChangePlayslistDoubleClick;
	}
}
