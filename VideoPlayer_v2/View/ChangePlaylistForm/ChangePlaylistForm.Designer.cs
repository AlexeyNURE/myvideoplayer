﻿namespace VideoPlayer_v2.View.ChangePlaylistForm
{
	partial class ChangePlaylistForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
			this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
			this.ChangeNameLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
			this.NewPlaylistNameTextbox = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
			this.PrivateCheckbox = new System.Windows.Forms.CheckBox();
			this.OldPasswordText = new Bunifu.Framework.UI.BunifuCustomLabel();
			this.OldPasswordTextbox = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
			this.NewPasswordLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
			this.newPasswordTextbox = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
			this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
			this.newPasswordConfirm = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
			this.PasswordPanel = new System.Windows.Forms.Panel();
			this.OldPasswordPanel = new System.Windows.Forms.Panel();
			this.SaveBtn = new Bunifu.Framework.UI.BunifuFlatButton();
			this.PasswordCancelBtn = new Bunifu.Framework.UI.BunifuFlatButton();
			this.RemoveBtn = new Bunifu.Framework.UI.BunifuFlatButton();
			this.PasswordPanel.SuspendLayout();
			this.OldPasswordPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// bunifuElipse1
			// 
			this.bunifuElipse1.ElipseRadius = 5;
			this.bunifuElipse1.TargetControl = this;
			// 
			// bunifuDragControl1
			// 
			this.bunifuDragControl1.Fixed = true;
			this.bunifuDragControl1.Horizontal = true;
			this.bunifuDragControl1.TargetControl = this;
			this.bunifuDragControl1.Vertical = true;
			// 
			// ChangeNameLabel
			// 
			this.ChangeNameLabel.AutoSize = true;
			this.ChangeNameLabel.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.ChangeNameLabel.Location = new System.Drawing.Point(12, 20);
			this.ChangeNameLabel.Name = "ChangeNameLabel";
			this.ChangeNameLabel.Size = new System.Drawing.Size(199, 30);
			this.ChangeNameLabel.TabIndex = 2;
			this.ChangeNameLabel.Text = "Change Name:";
			// 
			// NewPlaylistNameTextbox
			// 
			this.NewPlaylistNameTextbox.BorderColor = System.Drawing.Color.SeaGreen;
			this.NewPlaylistNameTextbox.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.NewPlaylistNameTextbox.Location = new System.Drawing.Point(17, 62);
			this.NewPlaylistNameTextbox.Multiline = true;
			this.NewPlaylistNameTextbox.Name = "NewPlaylistNameTextbox";
			this.NewPlaylistNameTextbox.Size = new System.Drawing.Size(338, 39);
			this.NewPlaylistNameTextbox.TabIndex = 3;
			this.NewPlaylistNameTextbox.Tag = "";
			// 
			// PrivateCheckbox
			// 
			this.PrivateCheckbox.AutoSize = true;
			this.PrivateCheckbox.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.PrivateCheckbox.Location = new System.Drawing.Point(17, 128);
			this.PrivateCheckbox.Name = "PrivateCheckbox";
			this.PrivateCheckbox.Size = new System.Drawing.Size(193, 34);
			this.PrivateCheckbox.TabIndex = 23;
			this.PrivateCheckbox.Text = "Make private";
			this.PrivateCheckbox.UseVisualStyleBackColor = true;
			this.PrivateCheckbox.CheckedChanged += new System.EventHandler(this.PrivateCheckbox_CheckedChanged);
			// 
			// OldPasswordText
			// 
			this.OldPasswordText.AutoSize = true;
			this.OldPasswordText.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.OldPasswordText.Location = new System.Drawing.Point(10, 14);
			this.OldPasswordText.Name = "OldPasswordText";
			this.OldPasswordText.Size = new System.Drawing.Size(172, 30);
			this.OldPasswordText.TabIndex = 25;
			this.OldPasswordText.Text = "Old Password";
			// 
			// OldPasswordTextbox
			// 
			this.OldPasswordTextbox.BorderColor = System.Drawing.Color.Chartreuse;
			this.OldPasswordTextbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.OldPasswordTextbox.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.OldPasswordTextbox.Location = new System.Drawing.Point(15, 47);
			this.OldPasswordTextbox.Multiline = true;
			this.OldPasswordTextbox.Name = "OldPasswordTextbox";
			this.OldPasswordTextbox.PasswordChar = '*';
			this.OldPasswordTextbox.Size = new System.Drawing.Size(345, 39);
			this.OldPasswordTextbox.TabIndex = 26;
			this.OldPasswordTextbox.Tag = "firstNameLabel";
			// 
			// NewPasswordLabel
			// 
			this.NewPasswordLabel.AutoSize = true;
			this.NewPasswordLabel.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.NewPasswordLabel.Location = new System.Drawing.Point(10, 10);
			this.NewPasswordLabel.Name = "NewPasswordLabel";
			this.NewPasswordLabel.Size = new System.Drawing.Size(183, 30);
			this.NewPasswordLabel.TabIndex = 27;
			this.NewPasswordLabel.Text = "New Password";
			// 
			// newPasswordTextbox
			// 
			this.newPasswordTextbox.BorderColor = System.Drawing.Color.Chartreuse;
			this.newPasswordTextbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.newPasswordTextbox.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.newPasswordTextbox.Location = new System.Drawing.Point(15, 43);
			this.newPasswordTextbox.Multiline = true;
			this.newPasswordTextbox.Name = "newPasswordTextbox";
			this.newPasswordTextbox.PasswordChar = '*';
			this.newPasswordTextbox.Size = new System.Drawing.Size(338, 39);
			this.newPasswordTextbox.TabIndex = 28;
			this.newPasswordTextbox.Tag = "firstNameLabel";
			// 
			// bunifuCustomLabel2
			// 
			this.bunifuCustomLabel2.AutoSize = true;
			this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.bunifuCustomLabel2.Location = new System.Drawing.Point(10, 98);
			this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
			this.bunifuCustomLabel2.Size = new System.Drawing.Size(286, 30);
			this.bunifuCustomLabel2.TabIndex = 29;
			this.bunifuCustomLabel2.Text = "Password Confirmation";
			// 
			// newPasswordConfirm
			// 
			this.newPasswordConfirm.BorderColor = System.Drawing.Color.SeaGreen;
			this.newPasswordConfirm.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.newPasswordConfirm.Location = new System.Drawing.Point(15, 131);
			this.newPasswordConfirm.Multiline = true;
			this.newPasswordConfirm.Name = "newPasswordConfirm";
			this.newPasswordConfirm.PasswordChar = '*';
			this.newPasswordConfirm.Size = new System.Drawing.Size(338, 39);
			this.newPasswordConfirm.TabIndex = 30;
			this.newPasswordConfirm.Tag = "firstNameLabel";
			// 
			// PasswordPanel
			// 
			this.PasswordPanel.BackColor = System.Drawing.Color.Transparent;
			this.PasswordPanel.Controls.Add(this.newPasswordConfirm);
			this.PasswordPanel.Controls.Add(this.bunifuCustomLabel2);
			this.PasswordPanel.Controls.Add(this.NewPasswordLabel);
			this.PasswordPanel.Controls.Add(this.newPasswordTextbox);
			this.PasswordPanel.Location = new System.Drawing.Point(12, 294);
			this.PasswordPanel.Name = "PasswordPanel";
			this.PasswordPanel.Size = new System.Drawing.Size(376, 190);
			this.PasswordPanel.TabIndex = 31;
			this.PasswordPanel.Visible = false;
			// 
			// OldPasswordPanel
			// 
			this.OldPasswordPanel.Controls.Add(this.OldPasswordText);
			this.OldPasswordPanel.Controls.Add(this.OldPasswordTextbox);
			this.OldPasswordPanel.Location = new System.Drawing.Point(12, 199);
			this.OldPasswordPanel.Name = "OldPasswordPanel";
			this.OldPasswordPanel.Size = new System.Drawing.Size(376, 89);
			this.OldPasswordPanel.TabIndex = 31;
			// 
			// SaveBtn
			// 
			this.SaveBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
			this.SaveBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
			this.SaveBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.SaveBtn.BorderRadius = 0;
			this.SaveBtn.ButtonText = "Save";
			this.SaveBtn.Cursor = System.Windows.Forms.Cursors.Hand;
			this.SaveBtn.DisabledColor = System.Drawing.Color.Gray;
			this.SaveBtn.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.SaveBtn.Iconcolor = System.Drawing.Color.Transparent;
			this.SaveBtn.Iconimage = null;
			this.SaveBtn.Iconimage_right = null;
			this.SaveBtn.Iconimage_right_Selected = null;
			this.SaveBtn.Iconimage_Selected = null;
			this.SaveBtn.IconMarginLeft = 0;
			this.SaveBtn.IconMarginRight = 0;
			this.SaveBtn.IconRightVisible = true;
			this.SaveBtn.IconRightZoom = 0D;
			this.SaveBtn.IconVisible = true;
			this.SaveBtn.IconZoom = 90D;
			this.SaveBtn.IsTab = false;
			this.SaveBtn.Location = new System.Drawing.Point(407, 347);
			this.SaveBtn.Margin = new System.Windows.Forms.Padding(5);
			this.SaveBtn.Name = "SaveBtn";
			this.SaveBtn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
			this.SaveBtn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
			this.SaveBtn.OnHoverTextColor = System.Drawing.Color.WhiteSmoke;
			this.SaveBtn.selected = false;
			this.SaveBtn.Size = new System.Drawing.Size(155, 61);
			this.SaveBtn.TabIndex = 32;
			this.SaveBtn.Text = "Save";
			this.SaveBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.SaveBtn.Textcolor = System.Drawing.Color.White;
			this.SaveBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
			// 
			// PasswordCancelBtn
			// 
			this.PasswordCancelBtn.Activecolor = System.Drawing.Color.Red;
			this.PasswordCancelBtn.BackColor = System.Drawing.Color.Red;
			this.PasswordCancelBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.PasswordCancelBtn.BorderRadius = 0;
			this.PasswordCancelBtn.ButtonText = "Cancel";
			this.PasswordCancelBtn.Cursor = System.Windows.Forms.Cursors.Hand;
			this.PasswordCancelBtn.DisabledColor = System.Drawing.Color.Gray;
			this.PasswordCancelBtn.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.PasswordCancelBtn.Iconcolor = System.Drawing.Color.Transparent;
			this.PasswordCancelBtn.Iconimage = null;
			this.PasswordCancelBtn.Iconimage_right = null;
			this.PasswordCancelBtn.Iconimage_right_Selected = null;
			this.PasswordCancelBtn.Iconimage_Selected = null;
			this.PasswordCancelBtn.IconMarginLeft = 0;
			this.PasswordCancelBtn.IconMarginRight = 0;
			this.PasswordCancelBtn.IconRightVisible = true;
			this.PasswordCancelBtn.IconRightZoom = 0D;
			this.PasswordCancelBtn.IconVisible = true;
			this.PasswordCancelBtn.IconZoom = 90D;
			this.PasswordCancelBtn.IsTab = false;
			this.PasswordCancelBtn.Location = new System.Drawing.Point(407, 423);
			this.PasswordCancelBtn.Margin = new System.Windows.Forms.Padding(5);
			this.PasswordCancelBtn.Name = "PasswordCancelBtn";
			this.PasswordCancelBtn.Normalcolor = System.Drawing.Color.Red;
			this.PasswordCancelBtn.OnHovercolor = System.Drawing.Color.Red;
			this.PasswordCancelBtn.OnHoverTextColor = System.Drawing.Color.WhiteSmoke;
			this.PasswordCancelBtn.selected = false;
			this.PasswordCancelBtn.Size = new System.Drawing.Size(155, 61);
			this.PasswordCancelBtn.TabIndex = 33;
			this.PasswordCancelBtn.Text = "Cancel";
			this.PasswordCancelBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.PasswordCancelBtn.Textcolor = System.Drawing.Color.White;
			this.PasswordCancelBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PasswordCancelBtn.Click += new System.EventHandler(this.PasswordCancelBtn_Click);
			// 
			// RemoveBtn
			// 
			this.RemoveBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.RemoveBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.RemoveBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.RemoveBtn.BorderRadius = 0;
			this.RemoveBtn.ButtonText = "Remove Playlist";
			this.RemoveBtn.Cursor = System.Windows.Forms.Cursors.Hand;
			this.RemoveBtn.DisabledColor = System.Drawing.Color.Gray;
			this.RemoveBtn.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.RemoveBtn.Iconcolor = System.Drawing.Color.Transparent;
			this.RemoveBtn.Iconimage = null;
			this.RemoveBtn.Iconimage_right = null;
			this.RemoveBtn.Iconimage_right_Selected = null;
			this.RemoveBtn.Iconimage_Selected = null;
			this.RemoveBtn.IconMarginLeft = 0;
			this.RemoveBtn.IconMarginRight = 0;
			this.RemoveBtn.IconRightVisible = true;
			this.RemoveBtn.IconRightZoom = 0D;
			this.RemoveBtn.IconVisible = true;
			this.RemoveBtn.IconZoom = 90D;
			this.RemoveBtn.IsTab = false;
			this.RemoveBtn.Location = new System.Drawing.Point(407, 267);
			this.RemoveBtn.Margin = new System.Windows.Forms.Padding(5);
			this.RemoveBtn.Name = "RemoveBtn";
			this.RemoveBtn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.RemoveBtn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.RemoveBtn.OnHoverTextColor = System.Drawing.Color.WhiteSmoke;
			this.RemoveBtn.selected = false;
			this.RemoveBtn.Size = new System.Drawing.Size(155, 61);
			this.RemoveBtn.TabIndex = 34;
			this.RemoveBtn.Text = "Remove Playlist";
			this.RemoveBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.RemoveBtn.Textcolor = System.Drawing.Color.White;
			this.RemoveBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.RemoveBtn.Click += new System.EventHandler(this.RemoveBtn_Click);
			// 
			// ChangePlaylistForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.ClientSize = new System.Drawing.Size(586, 513);
			this.Controls.Add(this.OldPasswordPanel);
			this.Controls.Add(this.RemoveBtn);
			this.Controls.Add(this.PasswordCancelBtn);
			this.Controls.Add(this.SaveBtn);
			this.Controls.Add(this.PasswordPanel);
			this.Controls.Add(this.PrivateCheckbox);
			this.Controls.Add(this.NewPlaylistNameTextbox);
			this.Controls.Add(this.ChangeNameLabel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "ChangePlaylistForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ChangePlaylistForm";
			this.PasswordPanel.ResumeLayout(false);
			this.PasswordPanel.PerformLayout();
			this.OldPasswordPanel.ResumeLayout(false);
			this.OldPasswordPanel.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
		private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
		private Bunifu.Framework.UI.BunifuCustomLabel ChangeNameLabel;
		private WindowsFormsControlLibrary1.BunifuCustomTextbox NewPlaylistNameTextbox;
		private System.Windows.Forms.CheckBox PrivateCheckbox;
		private Bunifu.Framework.UI.BunifuCustomLabel OldPasswordText;
		private WindowsFormsControlLibrary1.BunifuCustomTextbox OldPasswordTextbox;
		private Bunifu.Framework.UI.BunifuCustomLabel NewPasswordLabel;
		private WindowsFormsControlLibrary1.BunifuCustomTextbox newPasswordTextbox;
		private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
		private WindowsFormsControlLibrary1.BunifuCustomTextbox newPasswordConfirm;
		private System.Windows.Forms.Panel PasswordPanel;
		private Bunifu.Framework.UI.BunifuFlatButton SaveBtn;
		private Bunifu.Framework.UI.BunifuFlatButton PasswordCancelBtn;
		private Bunifu.Framework.UI.BunifuFlatButton RemoveBtn;
		private System.Windows.Forms.Panel OldPasswordPanel;
	}
}