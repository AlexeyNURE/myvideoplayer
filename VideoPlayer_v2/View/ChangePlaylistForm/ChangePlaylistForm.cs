﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsControlLibrary1;

namespace VideoPlayer_v2.View.ChangePlaylistForm
{
	public partial class ChangePlaylistForm : Form, IChangePlaylistView
	{
		public string OldPassword => OldPasswordTextbox.Text;

		public string NewPassword => newPasswordTextbox.Text;

		public string PasswordConfirm => newPasswordConfirm.Text;

		public string PlaylistName
		{
			get => NewPlaylistNameTextbox.Text;
			set => NewPlaylistNameTextbox.Text = value;
		}

		public CheckBox checkBox => PrivateCheckbox;

		public Panel passwordPanel => PasswordPanel;

		public Panel oldPasswordPanel => OldPasswordPanel;

		public ChangePlaylistForm()
		{
			InitializeComponent();
		}

		public event EventHandler SaveBtnClick;
		public event EventHandler CheckboxChanged;
		public event EventHandler RemoveBtnClick;

		public DialogResult ShowForm()
		{
			return ShowDialog();
		}

		public void CloseForm()
		{
			Close();
		}

		private void PrivateCheckbox_CheckedChanged(object sender, EventArgs e)
		{
			CheckboxChanged?.Invoke(sender, e);
		}

		private void PasswordCancelBtn_Click(object sender, EventArgs e)
		{
			CloseForm();
		}

		private void SaveBtn_Click(object sender, EventArgs e)
		{
			SaveBtnClick?.Invoke(sender, e);
		}

		private void RemoveBtn_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.No;
			RemoveBtnClick?.Invoke(sender, e);
		}
	}
}
