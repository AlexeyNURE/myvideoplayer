﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoPlayer_v2.View.PasswordForm
{
	public partial class PasswordForm : Form, IPasswordView
	{
		public string Password => PasswordTextBox.Text;

		public PasswordForm()
		{
			InitializeComponent();
		}

		public event EventHandler OkBtnClick;

		public DialogResult ShowForm()
		{
			return ShowDialog();
		}

		public void CloseForm()
		{
			Close();
		}

		private void PasswordOk_Click(object sender, EventArgs e)
		{
			OkBtnClick?.Invoke(sender, e);
		}

		private void CancelBtn_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			CloseForm();
		}
	}
}
