﻿namespace VideoPlayer_v2.View.PasswordForm
{
	partial class PasswordForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
			this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
			this.EnterPasswordLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
			this.PasswordTextBox = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
			this.PasswordOk = new Bunifu.Framework.UI.BunifuFlatButton();
			this.CancelBtn = new Bunifu.Framework.UI.BunifuFlatButton();
			this.SuspendLayout();
			// 
			// bunifuDragControl1
			// 
			this.bunifuDragControl1.Fixed = true;
			this.bunifuDragControl1.Horizontal = true;
			this.bunifuDragControl1.TargetControl = this;
			this.bunifuDragControl1.Vertical = true;
			// 
			// bunifuElipse1
			// 
			this.bunifuElipse1.ElipseRadius = 5;
			this.bunifuElipse1.TargetControl = this;
			// 
			// EnterPasswordLabel
			// 
			this.EnterPasswordLabel.AutoSize = true;
			this.EnterPasswordLabel.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.EnterPasswordLabel.Location = new System.Drawing.Point(12, 41);
			this.EnterPasswordLabel.Name = "EnterPasswordLabel";
			this.EnterPasswordLabel.Size = new System.Drawing.Size(190, 30);
			this.EnterPasswordLabel.TabIndex = 2;
			this.EnterPasswordLabel.Text = "Enter password";
			// 
			// PasswordTextBox
			// 
			this.PasswordTextBox.BorderColor = System.Drawing.Color.SeaGreen;
			this.PasswordTextBox.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.PasswordTextBox.Location = new System.Drawing.Point(12, 86);
			this.PasswordTextBox.Multiline = true;
			this.PasswordTextBox.Name = "PasswordTextBox";
			this.PasswordTextBox.PasswordChar = '*';
			this.PasswordTextBox.Size = new System.Drawing.Size(323, 39);
			this.PasswordTextBox.TabIndex = 3;
			this.PasswordTextBox.Tag = "";
			// 
			// PasswordOk
			// 
			this.PasswordOk.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
			this.PasswordOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
			this.PasswordOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.PasswordOk.BorderRadius = 0;
			this.PasswordOk.ButtonText = "OK";
			this.PasswordOk.Cursor = System.Windows.Forms.Cursors.Hand;
			this.PasswordOk.DisabledColor = System.Drawing.Color.Gray;
			this.PasswordOk.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.PasswordOk.Iconcolor = System.Drawing.Color.Transparent;
			this.PasswordOk.Iconimage = null;
			this.PasswordOk.Iconimage_right = null;
			this.PasswordOk.Iconimage_right_Selected = null;
			this.PasswordOk.Iconimage_Selected = null;
			this.PasswordOk.IconMarginLeft = 0;
			this.PasswordOk.IconMarginRight = 0;
			this.PasswordOk.IconRightVisible = true;
			this.PasswordOk.IconRightZoom = 0D;
			this.PasswordOk.IconVisible = true;
			this.PasswordOk.IconZoom = 90D;
			this.PasswordOk.IsTab = false;
			this.PasswordOk.Location = new System.Drawing.Point(12, 167);
			this.PasswordOk.Margin = new System.Windows.Forms.Padding(6);
			this.PasswordOk.Name = "PasswordOk";
			this.PasswordOk.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
			this.PasswordOk.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
			this.PasswordOk.OnHoverTextColor = System.Drawing.Color.WhiteSmoke;
			this.PasswordOk.selected = false;
			this.PasswordOk.Size = new System.Drawing.Size(142, 59);
			this.PasswordOk.TabIndex = 19;
			this.PasswordOk.Text = "OK";
			this.PasswordOk.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.PasswordOk.Textcolor = System.Drawing.Color.White;
			this.PasswordOk.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PasswordOk.Click += new System.EventHandler(this.PasswordOk_Click);
			// 
			// CancelBtn
			// 
			this.CancelBtn.Activecolor = System.Drawing.Color.Red;
			this.CancelBtn.BackColor = System.Drawing.Color.Red;
			this.CancelBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.CancelBtn.BorderRadius = 0;
			this.CancelBtn.ButtonText = "Cancel";
			this.CancelBtn.Cursor = System.Windows.Forms.Cursors.Hand;
			this.CancelBtn.DisabledColor = System.Drawing.Color.Gray;
			this.CancelBtn.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.CancelBtn.Iconcolor = System.Drawing.Color.Transparent;
			this.CancelBtn.Iconimage = null;
			this.CancelBtn.Iconimage_right = null;
			this.CancelBtn.Iconimage_right_Selected = null;
			this.CancelBtn.Iconimage_Selected = null;
			this.CancelBtn.IconMarginLeft = 0;
			this.CancelBtn.IconMarginRight = 0;
			this.CancelBtn.IconRightVisible = true;
			this.CancelBtn.IconRightZoom = 0D;
			this.CancelBtn.IconVisible = true;
			this.CancelBtn.IconZoom = 90D;
			this.CancelBtn.IsTab = false;
			this.CancelBtn.Location = new System.Drawing.Point(174, 167);
			this.CancelBtn.Margin = new System.Windows.Forms.Padding(6);
			this.CancelBtn.Name = "CancelBtn";
			this.CancelBtn.Normalcolor = System.Drawing.Color.Red;
			this.CancelBtn.OnHovercolor = System.Drawing.Color.Red;
			this.CancelBtn.OnHoverTextColor = System.Drawing.Color.WhiteSmoke;
			this.CancelBtn.selected = false;
			this.CancelBtn.Size = new System.Drawing.Size(161, 59);
			this.CancelBtn.TabIndex = 20;
			this.CancelBtn.Text = "Cancel";
			this.CancelBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.CancelBtn.Textcolor = System.Drawing.Color.White;
			this.CancelBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
			// 
			// PasswordForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.ClientSize = new System.Drawing.Size(350, 249);
			this.Controls.Add(this.CancelBtn);
			this.Controls.Add(this.PasswordOk);
			this.Controls.Add(this.PasswordTextBox);
			this.Controls.Add(this.EnterPasswordLabel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "PasswordForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "PasswordForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
		private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
		private Bunifu.Framework.UI.BunifuCustomLabel EnterPasswordLabel;
		private WindowsFormsControlLibrary1.BunifuCustomTextbox PasswordTextBox;
		private Bunifu.Framework.UI.BunifuFlatButton PasswordOk;
		private Bunifu.Framework.UI.BunifuFlatButton CancelBtn;
	}
}