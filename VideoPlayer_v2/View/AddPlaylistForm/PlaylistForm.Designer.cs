﻿namespace VideoPlayer_v2.View.AddPlaylistForm
{
	partial class PlaylistForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
			this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
			this.SaveBtn = new Bunifu.Framework.UI.BunifuFlatButton();
			this.PasswordCancelBtn = new Bunifu.Framework.UI.BunifuFlatButton();
			this.PlaylistNameTextbox = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
			this.PasswordTextbox = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
			this.PasswordText = new Bunifu.Framework.UI.BunifuCustomLabel();
			this.PasswordConfirm = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
			this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
			this.SuspendLayout();
			// 
			// bunifuElipse1
			// 
			this.bunifuElipse1.ElipseRadius = 5;
			this.bunifuElipse1.TargetControl = this;
			// 
			// bunifuCustomLabel1
			// 
			this.bunifuCustomLabel1.AutoSize = true;
			this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.bunifuCustomLabel1.Location = new System.Drawing.Point(12, 18);
			this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
			this.bunifuCustomLabel1.Size = new System.Drawing.Size(180, 30);
			this.bunifuCustomLabel1.TabIndex = 1;
			this.bunifuCustomLabel1.Text = "Playlist Name:";
			// 
			// SaveBtn
			// 
			this.SaveBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
			this.SaveBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
			this.SaveBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.SaveBtn.BorderRadius = 0;
			this.SaveBtn.ButtonText = "Save";
			this.SaveBtn.Cursor = System.Windows.Forms.Cursors.Hand;
			this.SaveBtn.DisabledColor = System.Drawing.Color.Gray;
			this.SaveBtn.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.SaveBtn.Iconcolor = System.Drawing.Color.Transparent;
			this.SaveBtn.Iconimage = null;
			this.SaveBtn.Iconimage_right = null;
			this.SaveBtn.Iconimage_right_Selected = null;
			this.SaveBtn.Iconimage_Selected = null;
			this.SaveBtn.IconMarginLeft = 0;
			this.SaveBtn.IconMarginRight = 0;
			this.SaveBtn.IconRightVisible = true;
			this.SaveBtn.IconRightZoom = 0D;
			this.SaveBtn.IconVisible = true;
			this.SaveBtn.IconZoom = 90D;
			this.SaveBtn.IsTab = false;
			this.SaveBtn.Location = new System.Drawing.Point(14, 431);
			this.SaveBtn.Margin = new System.Windows.Forms.Padding(5);
			this.SaveBtn.Name = "SaveBtn";
			this.SaveBtn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
			this.SaveBtn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
			this.SaveBtn.OnHoverTextColor = System.Drawing.Color.WhiteSmoke;
			this.SaveBtn.selected = false;
			this.SaveBtn.Size = new System.Drawing.Size(155, 61);
			this.SaveBtn.TabIndex = 18;
			this.SaveBtn.Text = "Save";
			this.SaveBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.SaveBtn.Textcolor = System.Drawing.Color.White;
			this.SaveBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.SaveBtn.Click += new System.EventHandler(this.SaveButton_Click);
			// 
			// PasswordCancelBtn
			// 
			this.PasswordCancelBtn.Activecolor = System.Drawing.Color.Red;
			this.PasswordCancelBtn.BackColor = System.Drawing.Color.Red;
			this.PasswordCancelBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.PasswordCancelBtn.BorderRadius = 0;
			this.PasswordCancelBtn.ButtonText = "Cancel";
			this.PasswordCancelBtn.Cursor = System.Windows.Forms.Cursors.Hand;
			this.PasswordCancelBtn.DisabledColor = System.Drawing.Color.Gray;
			this.PasswordCancelBtn.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.PasswordCancelBtn.Iconcolor = System.Drawing.Color.Transparent;
			this.PasswordCancelBtn.Iconimage = null;
			this.PasswordCancelBtn.Iconimage_right = null;
			this.PasswordCancelBtn.Iconimage_right_Selected = null;
			this.PasswordCancelBtn.Iconimage_Selected = null;
			this.PasswordCancelBtn.IconMarginLeft = 0;
			this.PasswordCancelBtn.IconMarginRight = 0;
			this.PasswordCancelBtn.IconRightVisible = true;
			this.PasswordCancelBtn.IconRightZoom = 0D;
			this.PasswordCancelBtn.IconVisible = true;
			this.PasswordCancelBtn.IconZoom = 90D;
			this.PasswordCancelBtn.IsTab = false;
			this.PasswordCancelBtn.Location = new System.Drawing.Point(200, 431);
			this.PasswordCancelBtn.Margin = new System.Windows.Forms.Padding(5);
			this.PasswordCancelBtn.Name = "PasswordCancelBtn";
			this.PasswordCancelBtn.Normalcolor = System.Drawing.Color.Red;
			this.PasswordCancelBtn.OnHovercolor = System.Drawing.Color.Red;
			this.PasswordCancelBtn.OnHoverTextColor = System.Drawing.Color.WhiteSmoke;
			this.PasswordCancelBtn.selected = false;
			this.PasswordCancelBtn.Size = new System.Drawing.Size(155, 61);
			this.PasswordCancelBtn.TabIndex = 19;
			this.PasswordCancelBtn.Text = "Cancel";
			this.PasswordCancelBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.PasswordCancelBtn.Textcolor = System.Drawing.Color.White;
			this.PasswordCancelBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PasswordCancelBtn.Click += new System.EventHandler(this.bunifuFlatButton1_Click);
			// 
			// PlaylistNameTextbox
			// 
			this.PlaylistNameTextbox.BorderColor = System.Drawing.Color.SeaGreen;
			this.PlaylistNameTextbox.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.PlaylistNameTextbox.Location = new System.Drawing.Point(17, 51);
			this.PlaylistNameTextbox.Multiline = true;
			this.PlaylistNameTextbox.Name = "PlaylistNameTextbox";
			this.PlaylistNameTextbox.Size = new System.Drawing.Size(338, 39);
			this.PlaylistNameTextbox.TabIndex = 1;
			this.PlaylistNameTextbox.Tag = "";
			// 
			// checkBox1
			// 
			this.checkBox1.AutoSize = true;
			this.checkBox1.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.checkBox1.Location = new System.Drawing.Point(17, 137);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(117, 34);
			this.checkBox1.TabIndex = 22;
			this.checkBox1.Text = "Private";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// bunifuDragControl1
			// 
			this.bunifuDragControl1.Fixed = true;
			this.bunifuDragControl1.Horizontal = true;
			this.bunifuDragControl1.TargetControl = this;
			this.bunifuDragControl1.Vertical = true;
			// 
			// PasswordTextbox
			// 
			this.PasswordTextbox.BorderColor = System.Drawing.Color.Chartreuse;
			this.PasswordTextbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.PasswordTextbox.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.PasswordTextbox.Location = new System.Drawing.Point(17, 256);
			this.PasswordTextbox.Multiline = true;
			this.PasswordTextbox.Name = "PasswordTextbox";
			this.PasswordTextbox.PasswordChar = '*';
			this.PasswordTextbox.Size = new System.Drawing.Size(338, 39);
			this.PasswordTextbox.TabIndex = 23;
			this.PasswordTextbox.Tag = "firstNameLabel";
			this.PasswordTextbox.Visible = false;
			// 
			// PasswordText
			// 
			this.PasswordText.AutoSize = true;
			this.PasswordText.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.PasswordText.Location = new System.Drawing.Point(12, 214);
			this.PasswordText.Name = "PasswordText";
			this.PasswordText.Size = new System.Drawing.Size(122, 30);
			this.PasswordText.TabIndex = 24;
			this.PasswordText.Text = "Password";
			this.PasswordText.Visible = false;
			// 
			// PasswordConfirm
			// 
			this.PasswordConfirm.BorderColor = System.Drawing.Color.SeaGreen;
			this.PasswordConfirm.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.PasswordConfirm.Location = new System.Drawing.Point(17, 363);
			this.PasswordConfirm.Multiline = true;
			this.PasswordConfirm.Name = "PasswordConfirm";
			this.PasswordConfirm.PasswordChar = '*';
			this.PasswordConfirm.Size = new System.Drawing.Size(338, 39);
			this.PasswordConfirm.TabIndex = 25;
			this.PasswordConfirm.Tag = "firstNameLabel";
			this.PasswordConfirm.Visible = false;
			// 
			// bunifuCustomLabel2
			// 
			this.bunifuCustomLabel2.AutoSize = true;
			this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.bunifuCustomLabel2.Location = new System.Drawing.Point(12, 312);
			this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
			this.bunifuCustomLabel2.Size = new System.Drawing.Size(286, 30);
			this.bunifuCustomLabel2.TabIndex = 26;
			this.bunifuCustomLabel2.Text = "Password Confirmation";
			this.bunifuCustomLabel2.Visible = false;
			// 
			// PlaylistForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.ClientSize = new System.Drawing.Size(390, 527);
			this.Controls.Add(this.bunifuCustomLabel2);
			this.Controls.Add(this.PasswordConfirm);
			this.Controls.Add(this.PasswordText);
			this.Controls.Add(this.PasswordTextbox);
			this.Controls.Add(this.checkBox1);
			this.Controls.Add(this.PlaylistNameTextbox);
			this.Controls.Add(this.PasswordCancelBtn);
			this.Controls.Add(this.SaveBtn);
			this.Controls.Add(this.bunifuCustomLabel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "PlaylistForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "PlaylistForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
		private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
		private Bunifu.Framework.UI.BunifuFlatButton SaveBtn;
		private Bunifu.Framework.UI.BunifuFlatButton PasswordCancelBtn;
		private WindowsFormsControlLibrary1.BunifuCustomTextbox PlaylistNameTextbox;
		private System.Windows.Forms.CheckBox checkBox1;
		private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
		private Bunifu.Framework.UI.BunifuCustomLabel PasswordText;
		private WindowsFormsControlLibrary1.BunifuCustomTextbox PasswordTextbox;
		private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
		private WindowsFormsControlLibrary1.BunifuCustomTextbox PasswordConfirm;
	}
}