﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoPlayer_v2.View.AddPlaylistForm
{
	public partial class PlaylistForm : Form, IPlaylistForm
	{
		public string Password => PasswordTextbox.Text;

		public string PlaylistName => PlaylistNameTextbox.Text;

		public bool isPrivate => checkBox1.Checked;

		private bool isPlaylistValid;

		public bool playlistValid
		{
			get => isPlaylistValid;
			set => isPlaylistValid = value;
		}

		string IPlaylistForm.PasswordConfirm => PasswordConfirm.Text;

		public event EventHandler SaveBtnClick;

		public PlaylistForm()
		{
			InitializeComponent();
			isPlaylistValid = false;
		}

		public DialogResult ShowForm()
		{
			return ShowDialog();
		}

		public void CloseForm()
		{
			Close();
		}

		private void SaveButton_Click(object sender, EventArgs e)
		{
			SaveBtnClick?.Invoke(sender, e);
			if (isPlaylistValid)
				CloseForm();
		}

		private void bunifuFlatButton1_Click(object sender, EventArgs e)
		{
			CloseForm();
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			if (checkBox1.Checked)
			{
				PasswordTextbox.Visible = true;
				PasswordText.Visible = true;
				bunifuCustomLabel2.Visible = true;
				PasswordConfirm.Visible = true;
			}
			else
			{
				PasswordTextbox.Visible = false;
				PasswordText.Visible = false;
				bunifuCustomLabel2.Visible = false;
				PasswordConfirm.Visible = false;
			}
		}
	}
}
