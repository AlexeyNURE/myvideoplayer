﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoPlayer_v2.View
{
	public interface IChangePlaylistView : IView
	{
		event EventHandler SaveBtnClick;
		event EventHandler CheckboxChanged;
		event EventHandler RemoveBtnClick;
		string PlaylistName { get; set; }
		string OldPassword { get; }
		string NewPassword { get; }
		string PasswordConfirm { get; }
		System.Windows.Forms.Panel passwordPanel { get; }
		System.Windows.Forms.CheckBox checkBox { get; }
		System.Windows.Forms.Panel oldPasswordPanel { get; }
	}
}
