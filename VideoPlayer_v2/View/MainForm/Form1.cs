﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace VideoPlayer_v2.View
{
	public partial class MainForm : Form, IMainForm, IMessageFilter
	{
		private Size m_panelSize { get; set; }

		public event EventHandler LoadMainForm;
		public event EventHandler ClosedForm;
		public event EventHandler BrowseClick;
		public event EventHandler PlayClick;
		public event EventHandler VideoSlide;
		public event EventHandler AudioSlide;
		public event EventHandler SoundClick;
		public event EventHandler VideoDoubleClick;
		public event EventHandler TimerTick;
		public event EventHandler StopClick;
		public event EventHandler EndClick;
		public event EventHandler FullScreenClick;
		public event EventHandler EscapeKey;
		public event EventHandler LeftKey;
		public event EventHandler RightKey;
		public event EventHandler SpaceKey;
		public event EventHandler UpKey;
		public event EventHandler DownKey;
		public event EventHandler AddPlaylistClick;
		public event EventHandler ChoosePlaylist;
		public event EventHandler AddVideoClick;
		public event EventHandler PathDoubleClick;
		public event EventHandler PrevButtonClick;
		public event EventHandler NextButtonClick;
		public event EventHandler ChangePlayslistDoubleClick;

		public int AudioSliderValue
		{
			get => sliderAudio.Value;
			set
			{
				if (value < 0 || value > 100)
					throw new InvalidEnumArgumentException("Invalid audio slider value");

				sliderAudio.Value = value;
			}
		}

		public AxWMPLib.AxWindowsMediaPlayer MediaPlayer
		{
			get => axWindowsMediaPlayer1;
			set
			{
				axWindowsMediaPlayer1 = value;
			}
		}

		public int VideoSliderValue
		{
			get => sliderVideo.Value;
			set
			{
				if (value < 0 || value > 100)
					throw new InvalidEnumArgumentException("Invalid audio slider value");

				sliderVideo.Value = value;
			}
		}

		public Size VideoPanelSize => m_panelSize;

		public ComboBox GetComboBox => PlaylistComboBox;

		public ListBox playlistBox { get => listBox1; }

		public Label nameLabel => PlaylistNameLabel;

		public MainForm()
		{
			InitializeComponent();
			AudioSliderValue = 50;
		}

		public DialogResult ShowForm()
		{
			return ShowDialog();
		}

		public void CloseForm()
		{
			Close();
		}

		public void changePlayImage(Bitmap _bitmap)
		{
			if (_bitmap == null)
				throw new ArgumentNullException();

			PlayBtn.Image = _bitmap;
		}

		public void changeSoundImage(Bitmap _bitmap)
		{
			if (_bitmap == null)
				throw new ArgumentNullException();

			SoundBtn.Image = _bitmap;
		}

		private void Form1_Load_1(object sender, EventArgs e)
		{
			Application.AddMessageFilter(this);
			LoadMainForm?.Invoke(sender, e);
		}

		private void bunifuImageButton1_Click_1(object sender, EventArgs e)
		{
			EndClick.Invoke(sender, e);
			Application.Exit();
		}

		private void bunifuFlatButton1_Click(object sender, EventArgs e)
		{
			BrowseClick.Invoke(sender, e);
		}

		private void Minimize_Click(object sender, EventArgs e)
		{
			this.WindowState = FormWindowState.Minimized;
		}

		private void bunifuSlider1_ValueChanged(object sender, EventArgs e)
		{
			VideoSlide.Invoke(sender, e);
		}

		public void playBtn_Click(object sender, EventArgs e)
		{
			timerVid.Enabled = true;
			PlayClick.Invoke(sender, e);
		}

		private void sliderAudio_ValueChanged(object sender, EventArgs e)
		{
			AudioSlide.Invoke(sender, e);

			if (AudioSliderValue == 0)
				SoundBtn.Image = Properties.Resources.NoSound_img;
			else
				SoundBtn.Image = Properties.Resources.Sound_img;
		}

		private void pnlVideo_DoubleClick(object sender, EventArgs e)
		{
			VideoDoubleClick.Invoke(sender, e);

			FormBorderStyle = FormBorderStyle.None;
			WindowState = FormWindowState.Maximized;
		}

		private void timerVid_Tick(object sender, EventArgs e)
		{
			TimerTick.Invoke(sender, e);
		}

		public void setLeftBoundText(string _text)
		{
			textVideoBeg.Text = _text;
		}

		public void setRightBoundText(string _text)
		{
			textVideoEnd.Text = _text;
		}

		private void BtnSound_Click(object sender, EventArgs e)
		{
			SoundClick.Invoke(sender, e);
		}

		private void stopBtn_Click(object sender, EventArgs e)
		{
			StopClick.Invoke(sender, e);
			sliderVideo.Value = 0;
			timerVid.Enabled = true;
			PlayBtn.Image = Properties.Resources.Play_img;
		}

		private void FullScreenBtn_Click(object sender, EventArgs e)
		{
			FullScreenClick.Invoke(sender, e);
		}

		public bool PreFilterMessage(ref Message m)
		{
			const UInt32 WM_KEYDOWN = 0x0100;

			if (m.Msg == WM_KEYDOWN)
			{
				Keys keyCode = (Keys)(int)m.WParam & Keys.KeyCode;

				switch (keyCode)
				{
					case Keys.Escape:
						EscapeKey.Invoke(null, EventArgs.Empty);
						return false;
					case Keys.Left:
						LeftKey.Invoke(null, EventArgs.Empty);
						return false;
					case Keys.Right:
						RightKey.Invoke(null, EventArgs.Empty);
						return false;
					case Keys.Space:
						SpaceKey.Invoke(null, EventArgs.Empty);
						return false;
					case Keys.Up:
						UpKey.Invoke(null, EventArgs.Empty);
						return false;
					case Keys.Down:
						DownKey.Invoke(null, EventArgs.Empty);
						return false;
					default:
						return false;
				}
			}

			return false;
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			Application.RemoveMessageFilter(this);
		}

		private void AddPlaylistBtn_Click(object sender, EventArgs e)
		{
			AddPlaylistClick.Invoke(sender, e);
		}

		private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
		{
			if (e.Index == -1)
				return;

			e.DrawBackground();

			Graphics g = e.Graphics;
			if (e.Index % 2 == 0)
				g.FillRectangle(new SolidBrush(Color.White), e.Bounds);
			else
				g.FillRectangle(new SolidBrush(Color.DimGray), e.Bounds);

			ListBox lb = (ListBox)sender;

			g.DrawString(
					lb.Items[e.Index].ToString()
				,	e.Font
				,	new SolidBrush(Color.Black)
				,	new PointF(e.Bounds.X, e.Bounds.Y)
			);

			e.DrawFocusRectangle();
		}

		private void PlaylistComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			ChoosePlaylist?.Invoke(sender, e);
		}

		private void bunifuFlatButton1_Click_1(object sender, EventArgs e)
		{
			AddVideoClick?.Invoke(sender, e);
		}

		private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			int index = listBox1.IndexFromPoint(e.Location);

			if (index != ListBox.NoMatches)
			{
				timerVid.Enabled = true;
				PathDoubleClick?.Invoke(sender, e);
			}
		}

		private void prevButton_Click(object sender, EventArgs e)
		{
			PrevButtonClick?.Invoke(sender, e);
		}

		private void nextButton_Click(object sender, EventArgs e)
		{
			NextButtonClick?.Invoke(sender, e);
		}

		private void PlaylistNameLabel_DoubleClick(object sender, EventArgs e)
		{
			ChangePlayslistDoubleClick?.Invoke(sender, e);
		}

		private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
		{
			ClosedForm?.Invoke(sender, e);
		}
	}
}
