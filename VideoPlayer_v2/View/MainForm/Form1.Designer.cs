﻿namespace VideoPlayer_v2.View
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
			this.Minimize_img = new Bunifu.Framework.UI.BunifuImageButton();
			this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
			this.panel1 = new System.Windows.Forms.Panel();
			this.PlayImg = new Bunifu.Framework.UI.BunifuImageButton();
			this.FullScreenBtn = new Bunifu.Framework.UI.BunifuImageButton();
			this.stopBtn = new Bunifu.Framework.UI.BunifuImageButton();
			this.SoundBtn = new Bunifu.Framework.UI.BunifuImageButton();
			this.sliderAudio = new Bunifu.Framework.UI.BunifuSlider();
			this.textVideoEnd = new Bunifu.Framework.UI.BunifuCustomLabel();
			this.textVideoBeg = new Bunifu.Framework.UI.BunifuCustomLabel();
			this.sliderVideo = new Bunifu.Framework.UI.BunifuSlider();
			this.PlayBtn = new Bunifu.Framework.UI.BunifuImageButton();
			this.bunifuImageButton6 = new Bunifu.Framework.UI.BunifuImageButton();
			this.bunifuImageButton5 = new Bunifu.Framework.UI.BunifuImageButton();
			this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.timerVid = new System.Windows.Forms.Timer(this.components);
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.PlaylistNameLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
			this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
			this.PlaylistComboBox = new System.Windows.Forms.ComboBox();
			this.AddPlaylistBtn = new Bunifu.Framework.UI.BunifuFlatButton();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.axWindowsMediaPlayer1 = new AxWMPLib.AxWindowsMediaPlayer();
			((System.ComponentModel.ISupportInitialize)(this.Minimize_img)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PlayImg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FullScreenBtn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.stopBtn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SoundBtn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PlayBtn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton5)).BeginInit();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).BeginInit();
			this.SuspendLayout();
			// 
			// bunifuElipse1
			// 
			this.bunifuElipse1.ElipseRadius = 5;
			this.bunifuElipse1.TargetControl = this;
			// 
			// Minimize_img
			// 
			this.Minimize_img.BackColor = System.Drawing.Color.Transparent;
			this.Minimize_img.Image = global::VideoPlayer_v2.Properties.Resources.Minimize_img;
			this.Minimize_img.ImageActive = null;
			this.Minimize_img.Location = new System.Drawing.Point(227, 0);
			this.Minimize_img.Name = "Minimize_img";
			this.Minimize_img.Size = new System.Drawing.Size(30, 25);
			this.Minimize_img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.Minimize_img.TabIndex = 4;
			this.Minimize_img.TabStop = false;
			this.Minimize_img.Zoom = 10;
			this.Minimize_img.Click += new System.EventHandler(this.Minimize_Click);
			// 
			// bunifuImageButton1
			// 
			this.bunifuImageButton1.BackColor = System.Drawing.Color.Transparent;
			this.bunifuImageButton1.Image = global::VideoPlayer_v2.Properties.Resources.Close_img;
			this.bunifuImageButton1.ImageActive = null;
			this.bunifuImageButton1.Location = new System.Drawing.Point(255, 0);
			this.bunifuImageButton1.Name = "bunifuImageButton1";
			this.bunifuImageButton1.Size = new System.Drawing.Size(30, 25);
			this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.bunifuImageButton1.TabIndex = 5;
			this.bunifuImageButton1.TabStop = false;
			this.bunifuImageButton1.Zoom = 10;
			this.bunifuImageButton1.Click += new System.EventHandler(this.bunifuImageButton1_Click_1);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Transparent;
			this.panel1.Controls.Add(this.PlayImg);
			this.panel1.Controls.Add(this.FullScreenBtn);
			this.panel1.Controls.Add(this.stopBtn);
			this.panel1.Controls.Add(this.SoundBtn);
			this.panel1.Controls.Add(this.sliderAudio);
			this.panel1.Controls.Add(this.textVideoEnd);
			this.panel1.Controls.Add(this.textVideoBeg);
			this.panel1.Controls.Add(this.sliderVideo);
			this.panel1.Controls.Add(this.PlayBtn);
			this.panel1.Controls.Add(this.bunifuImageButton6);
			this.panel1.Controls.Add(this.bunifuImageButton5);
			this.panel1.Location = new System.Drawing.Point(2, 412);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(811, 179);
			this.panel1.TabIndex = 7;
			// 
			// PlayImg
			// 
			this.PlayImg.BackColor = System.Drawing.Color.Transparent;
			this.PlayImg.Image = global::VideoPlayer_v2.Properties.Resources.Load_img;
			this.PlayImg.ImageActive = null;
			this.PlayImg.Location = new System.Drawing.Point(74, 63);
			this.PlayImg.Name = "PlayImg";
			this.PlayImg.Size = new System.Drawing.Size(57, 53);
			this.PlayImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.PlayImg.TabIndex = 11;
			this.PlayImg.TabStop = false;
			this.PlayImg.Zoom = 20;
			this.PlayImg.Click += new System.EventHandler(this.bunifuFlatButton1_Click);
			// 
			// FullScreenBtn
			// 
			this.FullScreenBtn.BackColor = System.Drawing.Color.Transparent;
			this.FullScreenBtn.Image = global::VideoPlayer_v2.Properties.Resources.Full_page_img;
			this.FullScreenBtn.ImageActive = null;
			this.FullScreenBtn.Location = new System.Drawing.Point(534, 60);
			this.FullScreenBtn.Name = "FullScreenBtn";
			this.FullScreenBtn.Size = new System.Drawing.Size(74, 55);
			this.FullScreenBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.FullScreenBtn.TabIndex = 14;
			this.FullScreenBtn.TabStop = false;
			this.FullScreenBtn.Zoom = 10;
			this.FullScreenBtn.Click += new System.EventHandler(this.FullScreenBtn_Click);
			// 
			// stopBtn
			// 
			this.stopBtn.BackColor = System.Drawing.Color.Transparent;
			this.stopBtn.Image = global::VideoPlayer_v2.Properties.Resources.Stop_img;
			this.stopBtn.ImageActive = null;
			this.stopBtn.Location = new System.Drawing.Point(162, 60);
			this.stopBtn.Name = "stopBtn";
			this.stopBtn.Size = new System.Drawing.Size(74, 55);
			this.stopBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.stopBtn.TabIndex = 13;
			this.stopBtn.TabStop = false;
			this.stopBtn.Zoom = 10;
			this.stopBtn.Click += new System.EventHandler(this.stopBtn_Click);
			// 
			// SoundBtn
			// 
			this.SoundBtn.BackColor = System.Drawing.Color.Transparent;
			this.SoundBtn.Image = global::VideoPlayer_v2.Properties.Resources.Sound_img;
			this.SoundBtn.ImageActive = null;
			this.SoundBtn.Location = new System.Drawing.Point(623, 59);
			this.SoundBtn.Name = "SoundBtn";
			this.SoundBtn.Size = new System.Drawing.Size(43, 36);
			this.SoundBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.SoundBtn.TabIndex = 11;
			this.SoundBtn.TabStop = false;
			this.SoundBtn.Zoom = 10;
			this.SoundBtn.Click += new System.EventHandler(this.BtnSound_Click);
			// 
			// sliderAudio
			// 
			this.sliderAudio.BackColor = System.Drawing.Color.Transparent;
			this.sliderAudio.BackgroudColor = System.Drawing.Color.DarkGray;
			this.sliderAudio.BorderRadius = 0;
			this.sliderAudio.IndicatorColor = System.Drawing.Color.SeaGreen;
			this.sliderAudio.Location = new System.Drawing.Point(673, 60);
			this.sliderAudio.Margin = new System.Windows.Forms.Padding(4);
			this.sliderAudio.MaximumValue = 100;
			this.sliderAudio.Name = "sliderAudio";
			this.sliderAudio.Size = new System.Drawing.Size(114, 30);
			this.sliderAudio.TabIndex = 7;
			this.sliderAudio.Value = 50;
			this.sliderAudio.ValueChanged += new System.EventHandler(this.sliderAudio_ValueChanged);
			// 
			// textVideoEnd
			// 
			this.textVideoEnd.AutoSize = true;
			this.textVideoEnd.BackColor = System.Drawing.Color.Transparent;
			this.textVideoEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textVideoEnd.Location = new System.Drawing.Point(737, 19);
			this.textVideoEnd.Name = "textVideoEnd";
			this.textVideoEnd.Size = new System.Drawing.Size(71, 20);
			this.textVideoEnd.TabIndex = 1;
			this.textVideoEnd.Text = "00:00:00";
			this.textVideoEnd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// textVideoBeg
			// 
			this.textVideoBeg.AutoSize = true;
			this.textVideoBeg.BackColor = System.Drawing.Color.Transparent;
			this.textVideoBeg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textVideoBeg.Location = new System.Drawing.Point(3, 19);
			this.textVideoBeg.Name = "textVideoBeg";
			this.textVideoBeg.Size = new System.Drawing.Size(71, 20);
			this.textVideoBeg.TabIndex = 0;
			this.textVideoBeg.Text = "00:00:00";
			// 
			// sliderVideo
			// 
			this.sliderVideo.BackColor = System.Drawing.Color.Transparent;
			this.sliderVideo.BackgroudColor = System.Drawing.Color.DarkGray;
			this.sliderVideo.BorderRadius = 0;
			this.sliderVideo.IndicatorColor = System.Drawing.Color.SeaGreen;
			this.sliderVideo.Location = new System.Drawing.Point(74, 17);
			this.sliderVideo.Margin = new System.Windows.Forms.Padding(4);
			this.sliderVideo.MaximumValue = 100;
			this.sliderVideo.Name = "sliderVideo";
			this.sliderVideo.Size = new System.Drawing.Size(661, 30);
			this.sliderVideo.TabIndex = 6;
			this.sliderVideo.Value = 0;
			this.sliderVideo.ValueChanged += new System.EventHandler(this.bunifuSlider1_ValueChanged);
			// 
			// PlayBtn
			// 
			this.PlayBtn.BackColor = System.Drawing.Color.Transparent;
			this.PlayBtn.Image = global::VideoPlayer_v2.Properties.Resources.Play_img;
			this.PlayBtn.ImageActive = null;
			this.PlayBtn.Location = new System.Drawing.Point(346, 60);
			this.PlayBtn.Name = "PlayBtn";
			this.PlayBtn.Size = new System.Drawing.Size(74, 55);
			this.PlayBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.PlayBtn.TabIndex = 8;
			this.PlayBtn.TabStop = false;
			this.PlayBtn.Zoom = 10;
			this.PlayBtn.Click += new System.EventHandler(this.playBtn_Click);
			// 
			// bunifuImageButton6
			// 
			this.bunifuImageButton6.BackColor = System.Drawing.Color.Transparent;
			this.bunifuImageButton6.Image = global::VideoPlayer_v2.Properties.Resources.Prev_img;
			this.bunifuImageButton6.ImageActive = null;
			this.bunifuImageButton6.Location = new System.Drawing.Point(253, 60);
			this.bunifuImageButton6.Name = "bunifuImageButton6";
			this.bunifuImageButton6.Size = new System.Drawing.Size(74, 55);
			this.bunifuImageButton6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.bunifuImageButton6.TabIndex = 10;
			this.bunifuImageButton6.TabStop = false;
			this.bunifuImageButton6.Zoom = 10;
			this.bunifuImageButton6.Click += new System.EventHandler(this.prevButton_Click);
			// 
			// bunifuImageButton5
			// 
			this.bunifuImageButton5.BackColor = System.Drawing.Color.Transparent;
			this.bunifuImageButton5.Image = global::VideoPlayer_v2.Properties.Resources.Next_img;
			this.bunifuImageButton5.ImageActive = null;
			this.bunifuImageButton5.Location = new System.Drawing.Point(444, 59);
			this.bunifuImageButton5.Name = "bunifuImageButton5";
			this.bunifuImageButton5.Size = new System.Drawing.Size(74, 55);
			this.bunifuImageButton5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.bunifuImageButton5.TabIndex = 9;
			this.bunifuImageButton5.TabStop = false;
			this.bunifuImageButton5.Zoom = 10;
			this.bunifuImageButton5.Click += new System.EventHandler(this.nextButton_Click);
			// 
			// bunifuDragControl1
			// 
			this.bunifuDragControl1.Fixed = true;
			this.bunifuDragControl1.Horizontal = true;
			this.bunifuDragControl1.TargetControl = this.textBox1;
			this.bunifuDragControl1.Vertical = true;
			// 
			// textBox1
			// 
			this.textBox1.BackColor = System.Drawing.Color.IndianRed;
			this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox1.Cursor = System.Windows.Forms.Cursors.Arrow;
			this.textBox1.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBox1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.textBox1.Location = new System.Drawing.Point(2, 0);
			this.textBox1.Margin = new System.Windows.Forms.Padding(2);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(220, 17);
			this.textBox1.TabIndex = 6;
			this.textBox1.Text = "VideoPlayer made by Alexeyz";
			this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// timerVid
			// 
			this.timerVid.Tick += new System.EventHandler(this.timerVid_Tick);
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.IndianRed;
			this.panel2.Controls.Add(this.textBox1);
			this.panel2.Controls.Add(this.bunifuImageButton1);
			this.panel2.Controls.Add(this.Minimize_img);
			this.panel2.Location = new System.Drawing.Point(802, 1);
			this.panel2.Margin = new System.Windows.Forms.Padding(2);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(288, 25);
			this.panel2.TabIndex = 8;
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.Transparent;
			this.panel3.Controls.Add(this.PlaylistNameLabel);
			this.panel3.Controls.Add(this.bunifuFlatButton1);
			this.panel3.Controls.Add(this.PlaylistComboBox);
			this.panel3.Controls.Add(this.AddPlaylistBtn);
			this.panel3.Controls.Add(this.listBox1);
			this.panel3.Location = new System.Drawing.Point(804, 31);
			this.panel3.Margin = new System.Windows.Forms.Padding(2);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(286, 560);
			this.panel3.TabIndex = 9;
			// 
			// PlaylistNameLabel
			// 
			this.PlaylistNameLabel.Font = new System.Drawing.Font("Century Gothic", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.PlaylistNameLabel.Location = new System.Drawing.Point(45, 87);
			this.PlaylistNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.PlaylistNameLabel.Name = "PlaylistNameLabel";
			this.PlaylistNameLabel.Size = new System.Drawing.Size(193, 34);
			this.PlaylistNameLabel.TabIndex = 21;
			this.PlaylistNameLabel.Text = "Playlist Name";
			this.PlaylistNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.PlaylistNameLabel.DoubleClick += new System.EventHandler(this.PlaylistNameLabel_DoubleClick);
			// 
			// bunifuFlatButton1
			// 
			this.bunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.bunifuFlatButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.bunifuFlatButton1.BorderRadius = 0;
			this.bunifuFlatButton1.ButtonText = "Add Video";
			this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
			this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
			this.bunifuFlatButton1.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
			this.bunifuFlatButton1.Iconimage = null;
			this.bunifuFlatButton1.Iconimage_right = null;
			this.bunifuFlatButton1.Iconimage_right_Selected = null;
			this.bunifuFlatButton1.Iconimage_Selected = null;
			this.bunifuFlatButton1.IconMarginLeft = 0;
			this.bunifuFlatButton1.IconMarginRight = 0;
			this.bunifuFlatButton1.IconRightVisible = true;
			this.bunifuFlatButton1.IconRightZoom = 0D;
			this.bunifuFlatButton1.IconVisible = true;
			this.bunifuFlatButton1.IconZoom = 90D;
			this.bunifuFlatButton1.IsTab = false;
			this.bunifuFlatButton1.Location = new System.Drawing.Point(-2, 517);
			this.bunifuFlatButton1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
			this.bunifuFlatButton1.Name = "bunifuFlatButton1";
			this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.WhiteSmoke;
			this.bunifuFlatButton1.selected = false;
			this.bunifuFlatButton1.Size = new System.Drawing.Size(284, 43);
			this.bunifuFlatButton1.TabIndex = 19;
			this.bunifuFlatButton1.Text = "Add Video";
			this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
			this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Century", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.bunifuFlatButton1.Click += new System.EventHandler(this.bunifuFlatButton1_Click_1);
			// 
			// PlaylistComboBox
			// 
			this.PlaylistComboBox.AllowDrop = true;
			this.PlaylistComboBox.BackColor = System.Drawing.Color.YellowGreen;
			this.PlaylistComboBox.DropDownHeight = 500;
			this.PlaylistComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.PlaylistComboBox.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.PlaylistComboBox.FormattingEnabled = true;
			this.PlaylistComboBox.IntegralHeight = false;
			this.PlaylistComboBox.ItemHeight = 25;
			this.PlaylistComboBox.Location = new System.Drawing.Point(116, 52);
			this.PlaylistComboBox.Margin = new System.Windows.Forms.Padding(2);
			this.PlaylistComboBox.Name = "PlaylistComboBox";
			this.PlaylistComboBox.Size = new System.Drawing.Size(160, 33);
			this.PlaylistComboBox.TabIndex = 18;
			this.PlaylistComboBox.SelectedIndexChanged += new System.EventHandler(this.PlaylistComboBox_SelectedIndexChanged);
			// 
			// AddPlaylistBtn
			// 
			this.AddPlaylistBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
			this.AddPlaylistBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
			this.AddPlaylistBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.AddPlaylistBtn.BorderRadius = 0;
			this.AddPlaylistBtn.ButtonText = "Add Playlist";
			this.AddPlaylistBtn.Cursor = System.Windows.Forms.Cursors.Hand;
			this.AddPlaylistBtn.DisabledColor = System.Drawing.Color.Gray;
			this.AddPlaylistBtn.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.AddPlaylistBtn.Iconcolor = System.Drawing.Color.Transparent;
			this.AddPlaylistBtn.Iconimage = null;
			this.AddPlaylistBtn.Iconimage_right = null;
			this.AddPlaylistBtn.Iconimage_right_Selected = null;
			this.AddPlaylistBtn.Iconimage_Selected = null;
			this.AddPlaylistBtn.IconMarginLeft = 0;
			this.AddPlaylistBtn.IconMarginRight = 0;
			this.AddPlaylistBtn.IconRightVisible = true;
			this.AddPlaylistBtn.IconRightZoom = 0D;
			this.AddPlaylistBtn.IconVisible = true;
			this.AddPlaylistBtn.IconZoom = 90D;
			this.AddPlaylistBtn.IsTab = false;
			this.AddPlaylistBtn.Location = new System.Drawing.Point(119, 3);
			this.AddPlaylistBtn.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
			this.AddPlaylistBtn.Name = "AddPlaylistBtn";
			this.AddPlaylistBtn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
			this.AddPlaylistBtn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
			this.AddPlaylistBtn.OnHoverTextColor = System.Drawing.Color.WhiteSmoke;
			this.AddPlaylistBtn.selected = false;
			this.AddPlaylistBtn.Size = new System.Drawing.Size(159, 44);
			this.AddPlaylistBtn.TabIndex = 17;
			this.AddPlaylistBtn.Text = "Add Playlist";
			this.AddPlaylistBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.AddPlaylistBtn.Textcolor = System.Drawing.Color.White;
			this.AddPlaylistBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.AddPlaylistBtn.Click += new System.EventHandler(this.AddPlaylistBtn_Click);
			// 
			// listBox1
			// 
			this.listBox1.BackColor = System.Drawing.SystemColors.ButtonShadow;
			this.listBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
			this.listBox1.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.listBox1.FormattingEnabled = true;
			this.listBox1.ItemHeight = 49;
			this.listBox1.Location = new System.Drawing.Point(0, 123);
			this.listBox1.Margin = new System.Windows.Forms.Padding(2);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(282, 396);
			this.listBox1.TabIndex = 16;
			this.listBox1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.listBox1_DrawItem);
			this.listBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseDoubleClick);
			// 
			// axWindowsMediaPlayer1
			// 
			this.axWindowsMediaPlayer1.Enabled = true;
			this.axWindowsMediaPlayer1.Location = new System.Drawing.Point(2, 1);
			this.axWindowsMediaPlayer1.Margin = new System.Windows.Forms.Padding(2);
			this.axWindowsMediaPlayer1.Name = "axWindowsMediaPlayer1";
			this.axWindowsMediaPlayer1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWindowsMediaPlayer1.OcxState")));
			this.axWindowsMediaPlayer1.Size = new System.Drawing.Size(799, 406);
			this.axWindowsMediaPlayer1.TabIndex = 0;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = global::VideoPlayer_v2.Properties.Resources.Background_img;
			this.ClientSize = new System.Drawing.Size(1091, 594);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.axWindowsMediaPlayer1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.KeyPreview = true;
			this.Margin = new System.Windows.Forms.Padding(2);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
			this.Load += new System.EventHandler(this.Form1_Load_1);
			((System.ComponentModel.ISupportInitialize)(this.Minimize_img)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.PlayImg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FullScreenBtn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.stopBtn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SoundBtn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PlayBtn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton5)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
		private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
		private Bunifu.Framework.UI.BunifuImageButton Minimize_img;
		private System.Windows.Forms.Panel panel1;
		private Bunifu.Framework.UI.BunifuImageButton stopBtn;
		private Bunifu.Framework.UI.BunifuImageButton SoundBtn;
		private Bunifu.Framework.UI.BunifuSlider sliderAudio;
		private Bunifu.Framework.UI.BunifuCustomLabel textVideoEnd;
		private Bunifu.Framework.UI.BunifuCustomLabel textVideoBeg;
		private Bunifu.Framework.UI.BunifuSlider sliderVideo;
		private Bunifu.Framework.UI.BunifuImageButton PlayBtn;
		private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton6;
		private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton5;
		private Bunifu.Framework.UI.BunifuImageButton PlayImg;
		private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
		private System.Windows.Forms.Timer timerVid;
		private AxWMPLib.AxWindowsMediaPlayer axWindowsMediaPlayer1;
		private Bunifu.Framework.UI.BunifuImageButton FullScreenBtn;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.ListBox listBox1;
		private Bunifu.Framework.UI.BunifuFlatButton AddPlaylistBtn;
		private System.Windows.Forms.ComboBox PlaylistComboBox;
		private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
		private Bunifu.Framework.UI.BunifuCustomLabel PlaylistNameLabel;
	}
}

