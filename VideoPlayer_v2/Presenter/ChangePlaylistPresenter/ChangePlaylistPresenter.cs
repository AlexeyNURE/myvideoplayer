﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoPlayer_v2.Presenter.ChangePlaylistPresenter
{
	class ChangePlaylistPresenter : IPresenter.IPresenter
	{
		//---------------------------------------------------------------------

		View.IChangePlaylistView m_changeForm;
		Model.Entity.Playlist m_playlist;

		List<string> m_playlistNames;

		//---------------------------------------------------------------------

		public ChangePlaylistPresenter(
				View.IChangePlaylistView _form
			,	Model.Entity.Playlist _playlist
			,	List<string> _playlistNames
		)
		{
			m_playlist = _playlist;
			m_changeForm = _form;
			m_playlistNames = _playlistNames;

			m_changeForm.PlaylistName = m_playlist.Name;

			if (m_playlist.isPrivate)
			{
				m_changeForm.passwordPanel.Visible = true;
				m_changeForm.checkBox.Text = "Make public";
			}
			else
			{
				m_changeForm.passwordPanel.Visible = false;
				m_changeForm.oldPasswordPanel.Visible = false;
				m_changeForm.checkBox.Text = "Make private";
			}

			subscribeEvents();
		}

		void subscribeEvents()
		{
			m_changeForm.SaveBtnClick += SaveBtnClick;
			m_changeForm.CheckboxChanged += CheckboxChanged;
			m_changeForm.RemoveBtnClick += RemoveBtnClick;
		}

		//---------------------------------------------------------------------

		private void RemoveBtnClick(object sender, EventArgs e)
		{
			m_changeForm.CloseForm();
		}

		private void CheckboxChanged(object sender, EventArgs e)
		{
			if (m_changeForm.checkBox.Checked)
			{
				if (m_playlist.isPrivate)
					m_changeForm.passwordPanel.Visible = false;
				else
					m_changeForm.passwordPanel.Visible = true;
			}
			else
			{
				if (m_playlist.isPrivate)
				{
					m_changeForm.passwordPanel.Visible = true;
					m_changeForm.oldPasswordPanel.Visible = true;
				}
				else
					m_changeForm.passwordPanel.Visible = false;
			}
		}

		private void SaveBtnClick(object sender, EventArgs e)
		{
			string message = "";
			if (isDataValid(ref message))
			{
				applyChanges();
				m_changeForm.CloseForm();
			}
			else
				MessageBox.Show(message);
		}

		private void applyChanges()
		{
			m_playlist.Name = m_changeForm.PlaylistName;

			if (!m_playlist.isPrivate)
			{
				if (m_changeForm.checkBox.Checked)
				{
					m_playlist.Password = m_changeForm.NewPassword;
				}
			}
			else
			{
				if (m_changeForm.checkBox.Checked)
					m_playlist.makePublic();
				else
				{
					if (!string.IsNullOrEmpty(m_changeForm.NewPassword))
						m_playlist.Password = m_changeForm.NewPassword;
				}
			}
		}

		bool isDataValid( ref string message )
		{
			if (
					m_playlist.isPrivate
				&&	m_changeForm.OldPassword != m_playlist.Password
			)
			{
				message = "Wrong old password!";
				return false;
			}
			else if (m_changeForm.NewPassword != m_changeForm.PasswordConfirm)
			{
				message = "Invalid password confirmation!";
				return false;
			}
			else if (m_playlist.isPrivate && !m_changeForm.checkBox.Checked && string.IsNullOrEmpty(m_changeForm.NewPassword))
			{
				message = "Empty password!";
				return false;
			}
			else if (!m_playlist.isPrivate && m_changeForm.checkBox.Checked && string.IsNullOrEmpty(m_changeForm.NewPassword))
			{
				message = "Empty password!";
				return false;
			}
			else if (
					m_playlist.Name != m_changeForm.PlaylistName
				&&	m_playlistNames.Contains(m_changeForm.PlaylistName)
			)
			{
				message = "Such name already exists";
				return false;
			}

			return true;
		}

		public void Run()
		{
			m_changeForm.ShowForm();
		}
	}
}
