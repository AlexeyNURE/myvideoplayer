﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoPlayer_v2.Presenter.PlaylistPresenter
{
	class PlaylistPresenter : IPresenter.IPresenter
	{
		//---------------------------------------------------------------------

		private readonly View.IPlaylistForm m_playlistForm;

		private List<string> m_playlistNames;

		//---------------------------------------------------------------------

		public PlaylistPresenter( View.IPlaylistForm _form, List<string> _names )
		{
			if (_names == null)
				throw new ArgumentException("List is null");

			m_playlistNames = _names;

			m_playlistForm = _form;
			subscribeEvents();
		}

		private void subscribeEvents()
		{
			m_playlistForm.SaveBtnClick += SaveBtnClick;
		}

		//---------------------------------------------------------------------

		private void SaveBtnClick(object sender, EventArgs e)
		{
			string message = "";
			if (isDataValid(ref message))
				m_playlistForm.playlistValid = true;
			else
				MessageBox.Show(message);
		}

		bool isDataValid(ref string _message)
		{
			if (string.IsNullOrEmpty(m_playlistForm.PlaylistName))
			{
				_message = "Name shound not be empty!";
				return false;
			}

			if (m_playlistForm.isPrivate && string.IsNullOrEmpty(m_playlistForm.Password))
			{
				_message = "Password shound not be empty";
				return false;
			}

			if (m_playlistNames.Contains(m_playlistForm.PlaylistName))
			{
				_message = "Name " + m_playlistForm.PlaylistName + " already exists";
				return false;
			}

			if ( m_playlistForm.Password != m_playlistForm.PasswordConfirm )
			{
				_message = "Password doesn't match confirmation. Please try again!";
				return false;
			}

			return true;
		}

		public Model.Entity.Playlist GetPlaylist()
		{
			if (!m_playlistForm.playlistValid)
				return null;

			Model.Entity.Playlist playlist = new Model.Entity.Playlist(m_playlistForm.PlaylistName);
			if ( m_playlistForm.isPrivate )
				playlist.Password = m_playlistForm.Password;

			return playlist;
		}

		public void Run()
		{
			m_playlistForm.ShowForm();
		}


	}
}
