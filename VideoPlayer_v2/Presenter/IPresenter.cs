﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoPlayer_v2.IPresenter
{
	public interface IPresenter
	{
		void Run();
	}
}
