﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoPlayer_v2.Presenter.MainPresenter
{
	class MainPresenter : IPresenter.IPresenter
	{
		//---------------------------------------------------------------------
		#region fields
		private readonly View.IMainForm m_mainForm;
		private readonly Model.VideoPlayer m_videoPlayer;

		private Dictionary<string, Model.Entity.IPlaylist> m_playlistList;

		private Model.Entity.IPlaylist m_currentPlaylist;

		private Model.Entity.HistoryPlaylist m_historyPlaylist;

		private int m_lastSoundValue;

		private string m_currentFileName;
		#endregion
		//---------------------------------------------------------------------

		public MainPresenter(View.IMainForm _mainForm, Model.VideoPlayer _player)
		{
			m_mainForm = _mainForm;
			m_videoPlayer = _player;
			m_playlistList = new Dictionary<string, Model.Entity.IPlaylist>();
			m_historyPlaylist = new Model.Entity.HistoryPlaylist();

			m_lastSoundValue = 50;

			subscribeEvents();
		}

		//---------------------------------------------------------------------

		private void subscribeEvents()
		{
			m_mainForm.LoadMainForm += m_mainFormLoad;
			m_mainForm.ClosedForm += Form_Closed;
			m_mainForm.AudioSlide += AudioSlide;
			m_mainForm.BrowseClick += BrowseClick;
			m_mainForm.PlayClick += PlayClick;
			m_mainForm.SoundClick += SoundClick;
			m_mainForm.VideoSlide += VideoSlide;
			m_mainForm.TimerTick += TimerTick;
			m_mainForm.StopClick += StopClick;
			m_mainForm.EndClick += EndClick;
			m_mainForm.FullScreenClick += FullScreenClick;
			m_mainForm.EscapeKey += EscapeKey;
			m_mainForm.LeftKey += LeftKey;
			m_mainForm.RightKey += RightKey;
			m_mainForm.SpaceKey += SpaceKey;
			m_mainForm.UpKey += UpKey;
			m_mainForm.DownKey += DownKey;
			m_mainForm.AddPlaylistClick += AddPlaylistClick;
			m_mainForm.ChoosePlaylist += ChoosePlaylist;
			m_mainForm.AddVideoClick += AddVideoClick;
			m_mainForm.PathDoubleClick += PathDoubleClick;
			m_mainForm.PrevButtonClick += PrevButtonClick;
			m_mainForm.NextButtonClick += NextButtonClick;
			m_mainForm.ChangePlayslistDoubleClick += M_mainForm_ChangePlayslistDoubleClick;
		}

		//---------------------------------------------------------------------
		#region events
		private void Form_Closed(object sender, EventArgs e)
		{
			Model.SaveRestore.FileProcessor.Save("../../../saver.txt", m_playlistList);
		}

		private void m_mainFormLoad(object sender, EventArgs e)
		{
			m_playlistList = (Dictionary<string, Model.Entity.IPlaylist>)Model
					.SaveRestore
					.FileProcessor
					.Restore("../../../saver.txt")
				??	new Dictionary<string, Model.Entity.IPlaylist>();

			m_playlistList[m_historyPlaylist.Name] = m_historyPlaylist;
			m_currentPlaylist = m_historyPlaylist;
			m_mainForm.nameLabel.Text = m_currentPlaylist.Name;

			if (m_playlistList.Count != 0)
			{
				m_mainForm.GetComboBox.Items.AddRange(m_playlistList.Keys.ToArray());
			}

			m_mainForm.GetComboBox.SelectedIndex = 0;
		}

		//---------------------------------------------------------------------

		private void M_mainForm_ChangePlayslistDoubleClick(object sender, EventArgs e)
		{
			if (m_currentPlaylist is Model.Entity.HistoryPlaylist)
			{
				MessageBox.Show("You can't change history!");
				return;
			}

			string prevName = m_mainForm.nameLabel.Text;
			m_mainForm.nameLabel.Text = m_mainForm.GetComboBox.Text;

			View.ChangePlaylistForm.ChangePlaylistForm changePlaylistForm = 
				new View.ChangePlaylistForm.ChangePlaylistForm();

			ChangePlaylistPresenter.ChangePlaylistPresenter changePlaylistPresenter
				= new ChangePlaylistPresenter.ChangePlaylistPresenter(
						changePlaylistForm
					,	(Model.Entity.Playlist)m_currentPlaylist
					,	m_playlistList.Keys.ToList()
				);

			var res = changePlaylistForm.ShowForm();

			if (res == DialogResult.No)
			{
				m_mainForm.playlistBox.Items.Clear();
				m_playlistList.Remove(m_currentPlaylist.Name);
				m_mainForm.GetComboBox.Items.Remove(m_currentPlaylist.Name);
				m_currentPlaylist = m_historyPlaylist;
				m_mainForm.GetComboBox.SelectedIndex = 0;
				foreach (var path in m_currentPlaylist)
				{
					m_mainForm.playlistBox.Items.Add(Path.GetFileNameWithoutExtension(path));
				}
			}
			else
				RenameKey(m_playlistList, prevName, m_currentPlaylist.Name);

			m_mainForm.nameLabel.Text = m_currentPlaylist.Name;
			m_mainForm.GetComboBox.Items[m_mainForm.GetComboBox.SelectedIndex] = m_currentPlaylist.Name;
		}

		private void NextButtonClick(object sender, EventArgs e)
		{
			if (m_mainForm.playlistBox.SelectedIndex == m_currentPlaylist.getSize() - 1)
				m_mainForm.playlistBox.SelectedIndex = 0;
			else
				m_mainForm.playlistBox.SelectedIndex++;

			playVideoByIdx(m_mainForm.playlistBox.SelectedIndex);
		}

		private void PrevButtonClick(object sender, EventArgs e)
		{
			if (m_mainForm.playlistBox.SelectedIndex == 0)
				m_mainForm.playlistBox.SelectedIndex = m_currentPlaylist.getSize() - 1;
			else
				m_mainForm.playlistBox.SelectedIndex--;

			playVideoByIdx(m_mainForm.playlistBox.SelectedIndex);
		}

		private void PathDoubleClick(object sender, EventArgs e)
		{
			var me = e as MouseEventArgs;
			int index = m_mainForm.playlistBox.IndexFromPoint(me.Location);

			playVideoByIdx(index);
		}

		private void playVideoByIdx( int _index )
		{
			if (_index >= m_currentPlaylist.getSize() || _index < 0)
				return;

			if (_index != System.Windows.Forms.ListBox.NoMatches)
			{
				m_videoPlayer.setPlayer(m_mainForm.MediaPlayer);
				m_currentFileName = m_currentPlaylist[_index];
				m_videoPlayer.LoadVideo(m_currentPlaylist[_index]);
				m_mainForm.playlistBox.SelectedIndex = _index;
				PlayClick(null, EventArgs.Empty);
			}
		}

		private void AddVideoClick(object sender, EventArgs e)
		{
			if (m_currentPlaylist is Model.Entity.HistoryPlaylist)
			{
				MessageBox.Show("You can't add video to history!");
				return;
			}
			OpenFileDialog openFileDialog1 = new OpenFileDialog();

			openFileDialog1.InitialDirectory = "C:\\Users\\oleks\\Downloads\\";
			openFileDialog1.Filter = "Media files|*.wmv;*.wmv;*.mp4";
			openFileDialog1.FilterIndex = 0;
			openFileDialog1.RestoreDirectory = true;

			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				string m_currentFile = openFileDialog1.FileName;
				string name = Path.GetFileNameWithoutExtension(m_currentFile);
				m_currentPlaylist.addVideoPath(m_currentFile);
				m_mainForm.playlistBox.Items.Add(name);
			}
		}

		private void ChoosePlaylist(object sender, EventArgs e)
		{
			string prevName = m_mainForm.nameLabel.Text;
			m_mainForm.nameLabel.Text = m_mainForm.GetComboBox.Text;

			if (m_currentPlaylist.Name == m_mainForm.GetComboBox.Text)
				return;

			if (
					m_playlistList[m_mainForm.nameLabel.Text] is Model.Entity.Playlist p
				&&	p.isPrivate
			)
			{
				View.PasswordForm.PasswordForm passwordForm = new View.PasswordForm.PasswordForm();

				PasswordPresenter.PasswordPresenter passwordPresenter
					= new PasswordPresenter.PasswordPresenter(passwordForm, p.Password);

				var res = passwordForm.ShowForm();

				if (res == DialogResult.Cancel)
				{
					m_mainForm.nameLabel.Text = prevName;
					m_mainForm.GetComboBox.Text = prevName;
				}
			}

			m_mainForm.playlistBox.Items.Clear();

			m_currentPlaylist = m_playlistList[m_mainForm.nameLabel.Text];

			foreach(var path in m_currentPlaylist)
			{
				m_mainForm.playlistBox.Items.Add(Path.GetFileNameWithoutExtension(path));
			}
		}

		private void AddPlaylistClick(object sender, EventArgs e)
		{
			View.AddPlaylistForm.PlaylistForm playlistForm = new View.AddPlaylistForm.PlaylistForm();

			Presenter.PlaylistPresenter.PlaylistPresenter playlistPresenter
				= new PlaylistPresenter.PlaylistPresenter(playlistForm, m_playlistList.Keys.ToList());

			playlistPresenter.Run();

			addItemToPlaylist(playlistPresenter.GetPlaylist());
		}

		public void addItemToPlaylist(Model.Entity.IPlaylist _playlist)
		{
			if (_playlist == null)
				return;

			string playlistName = _playlist.Name;

			m_playlistList.Add(playlistName, _playlist);

			m_mainForm.GetComboBox.Items.Add(playlistName);
		}

		private void DownKey(object sender, EventArgs e)
		{
			if (!m_videoPlayer.isValid())
				return;

			int position = m_videoPlayer.GetVideo.settings.volume - 5;
			setAudioPosition(position);
		}

		private void UpKey(object sender, EventArgs e)
		{
			if (!m_videoPlayer.isValid())
				return;

			int position = m_videoPlayer.GetVideo.settings.volume + 5;
			setAudioPosition(position);
		}

		private void SpaceKey(object sender, EventArgs e)
		{
			PlayClick(sender, e);
		}

		private void RightKey(object sender, EventArgs e)
		{
			if (!m_videoPlayer.isValid())
				return;

			double position = m_videoPlayer.GetVideo.Ctlcontrols.currentPosition + 5;
			setVideoPosition(position);
			m_videoPlayer.GetVideo.Ctlcontrols.currentPosition = position;
		}

		private void LeftKey(object sender, EventArgs e)
		{
			if (!m_videoPlayer.isValid())
				return;

			double position = m_videoPlayer.GetVideo.Ctlcontrols.currentPosition - 5;
			setVideoPosition(position);
			m_videoPlayer.GetVideo.Ctlcontrols.currentPosition = position;
		}

		private void EscapeKey(object sender, EventArgs e)
		{
			if (!m_videoPlayer.isValid())
				return;

			if (m_videoPlayer.GetVideo.fullScreen == true)
				m_videoPlayer.GetVideo.fullScreen = false;
		}

		private void FullScreenClick(object sender, EventArgs e)
		{
			if (m_videoPlayer.isPlaying())
			{
				if (m_videoPlayer.GetVideo.fullScreen == false)
					m_videoPlayer.GetVideo.fullScreen = true;
				else
					m_videoPlayer.GetVideo.fullScreen = false;
			}
		}

		private void VideoDoubleClick(object sender, AxWMPLib._WMPOCXEvents_DoubleClickEvent e)
		{
			if (m_videoPlayer.isPlaying())
			{
				if (m_videoPlayer.GetVideo.fullScreen == false)
					m_videoPlayer.GetVideo.fullScreen = true;
				else
					m_videoPlayer.GetVideo.fullScreen = false;
			}
		}

		private void EndClick(object sender, EventArgs e)
		{
			m_videoPlayer.stopVideo();
		}

		private void StopClick(object sender, EventArgs e)
		{
			m_videoPlayer.SlideVideo(0);
			m_videoPlayer.Pause();
			m_mainForm.setLeftBoundText("00:00:00");
		}

		private void AudioSlide(object sender, EventArgs e)
		{
			int value = m_mainForm.AudioSliderValue;
			m_videoPlayer.SlideAudio(value);
		}
		private void VideoSlide(object sender, EventArgs e)
		{
			int value = m_mainForm.VideoSliderValue;
			m_videoPlayer.SlideVideo(value);
		}

		private void setAudioPosition( int _position )
		{
			if (!m_videoPlayer.isPlaying())
				return;

			if (_position < 0)
				_position = 0;

			if (_position > 100)
				_position = 100;

			m_videoPlayer.SlideAudio(_position);

			if (_position == 0)
				m_mainForm.changeSoundImage(Properties.Resources.NoSound_img);
			else
				m_mainForm.changeSoundImage(Properties.Resources.Sound_img);

			m_mainForm.AudioSliderValue = _position;
		}

		private void setVideoPosition( double _position )
		{
			if (!m_videoPlayer.isPlaying())
				return;

			double duration = m_videoPlayer.GetVideo.Ctlcontrols.currentItem.duration;

			if (_position < 0)
				_position = 0.0;

			if (_position > duration)
				_position = duration;

			int currentTime = Convert.ToInt32(_position);
			int maxTime = Convert.ToInt32(duration);

			string leftBound =
				string.Format(
						"{0:00}:{1:00}:{2:00}"
					,	currentTime / 3600
					,	(currentTime / 60) % 60
					,	currentTime % 60
				);
			
			string rightBound =
				string.Format(
						"{0:00}:{1:00}:{2:00}"
					,	maxTime / 3600
					,	(maxTime / 60) % 60
					,	maxTime % 60
				);

			m_mainForm.setLeftBoundText(leftBound);
			m_mainForm.setRightBoundText(rightBound);

			int value = currentTime * 100 / maxTime;

			m_mainForm.VideoSliderValue = value;
		}
		private void BrowseClick(object sender, EventArgs e)
		{
			OpenFileDialog openFileDialog1 = new OpenFileDialog();

			openFileDialog1.InitialDirectory = "C:\\Users\\oleks\\Downloads\\";
			openFileDialog1.Filter = "Media files|*.wmv;*.wmv;*.mp4";
			openFileDialog1.FilterIndex = 0;
			openFileDialog1.RestoreDirectory = true;

			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				m_currentFileName = openFileDialog1.FileName;
				m_videoPlayer.setPlayer(m_mainForm.MediaPlayer);
				m_videoPlayer.LoadVideo(m_currentFileName);
				m_videoPlayer.SlideAudio(m_lastSoundValue);
				PlayClick(null, EventArgs.Empty);
				string name = Path.GetFileNameWithoutExtension(m_currentFileName);

				m_mainForm.playlistBox.Items.Clear();
				foreach (var path in m_currentPlaylist)
				{
					m_mainForm.playlistBox.Items.Add(Path.GetFileNameWithoutExtension(path));
				}
			}
		}

		private void PlayClick(object sender, EventArgs e)
		{
			if (!m_videoPlayer.isValid())
				return;

			if (m_videoPlayer.isPlaying())
			{
				m_videoPlayer.Pause();
				m_mainForm.changePlayImage(Properties.Resources.Play_img);
			}
			else
			{
				m_videoPlayer.Play();

				if (!m_historyPlaylist.hasPath(m_currentFileName))
					m_historyPlaylist.addVideoPath(m_currentFileName);

				m_mainForm.changePlayImage(Properties.Resources.Pause_img);
			}
		}

		private void SoundClick(object sender, EventArgs e)
		{
			if (!m_videoPlayer.isValid())
				return;

			if (m_videoPlayer.getVolume() == 0)
			{
				m_videoPlayer.SlideAudio(m_lastSoundValue);
				m_mainForm.changeSoundImage(Properties.Resources.Sound_img);
				m_mainForm.AudioSliderValue = m_lastSoundValue;
			}
			else
			{
				m_lastSoundValue = m_videoPlayer.getVolume();
				m_videoPlayer.SlideAudio(0);
				m_mainForm.changeSoundImage(Properties.Resources.NoSound_img);
				m_mainForm.AudioSliderValue = 0;
			}
		}

		private void TimerTick(object sender, EventArgs e)
		{
			if ( m_videoPlayer.isPlaying() )
				setVideoPosition(m_videoPlayer.GetVideo.Ctlcontrols.currentPosition);
		}
		#endregion
		//---------------------------------------------------------------------

		public void Run()
		{
			m_mainForm.ShowForm();
		}

		//---------------------------------------------------------------------

		private static void RenameKey<TKey, TValue>(
				IDictionary<TKey, TValue> dic
			,	TKey fromKey, TKey toKey
		)
		{
			TValue value = dic[fromKey];
			dic.Remove(fromKey);
			dic[toKey] = value;
		}
	}
}
