﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoPlayer_v2.Presenter.PasswordPresenter
{
	class PasswordPresenter : IPresenter.IPresenter
	{
		//---------------------------------------------------------------------

		private readonly View.IPasswordView m_passwordForm;

		private string m_password;

		//---------------------------------------------------------------------

		public PasswordPresenter(View.IPasswordView _form, string _password)
		{
			if (_password == null)
				throw new ArgumentException("password is null");

			m_password = _password;

			m_passwordForm = _form;
			subscribeEvents();
		}

		private void subscribeEvents()
		{
			m_passwordForm.OkBtnClick += OkBtnClick;
		}

		//---------------------------------------------------------------------

		private void OkBtnClick(object sender, EventArgs e)
		{
			if (m_password == m_passwordForm.Password)
			{
				((View.PasswordForm.PasswordForm)m_passwordForm).DialogResult = DialogResult.OK;
				m_passwordForm.CloseForm();
			}
			else
				MessageBox.Show("Wrong password!");
		}

		public void Run()
		{
			m_passwordForm.ShowForm();
		}
	}
}
