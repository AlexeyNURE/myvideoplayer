﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoPlayer_v2
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Model.VideoPlayer player = new Model.VideoPlayer();
			View.MainForm form = new View.MainForm();

			Presenter.MainPresenter.MainPresenter presenter =
				new Presenter.MainPresenter.MainPresenter(form, player);

			Application.Run(form);
		}
	}
}
