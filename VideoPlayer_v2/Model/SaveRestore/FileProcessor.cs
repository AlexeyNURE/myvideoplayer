﻿using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace VideoPlayer_v2.Model.SaveRestore
{
	public static class FileProcessor
	{
		//---------------------------------------------------------------------

		public static void Save(string _path, object _object)
		{
			using (FileStream fileStream = new FileStream(_path, FileMode.OpenOrCreate))
			{
				BinaryFormatter binaryFormatter = new BinaryFormatter();

				binaryFormatter.Serialize(fileStream, _object);
			}
		}

		//---------------------------------------------------------------------

		public static object Restore(string _path)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();

			object result = new object();

			using (FileStream file = new FileStream(_path, FileMode.OpenOrCreate))
			{
				if (file.Length == 0)
					return null;

				result = binaryFormatter.Deserialize(file);

				return result;
			}
		}

		//---------------------------------------------------------------------
	}
}
