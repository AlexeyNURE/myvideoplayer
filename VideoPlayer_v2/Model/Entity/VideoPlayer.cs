﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using WMPLib;
using AxWMPLib;

namespace VideoPlayer_v2.Model
{
	public class VideoPlayer
	{
		//---------------------------------------------------------------------

		private AxWindowsMediaPlayer m_video { get; set; }

		//---------------------------------------------------------------------

		public AxWindowsMediaPlayer GetVideo
		{
			get
			{
				if (!isValid())
					throw new NullReferenceException();

				return m_video;
			}
		}

		//---------------------------------------------------------------------

		public VideoPlayer()
		{ }

		public void LoadVideo(string _fileName)
		{
			m_video.URL = _fileName;
		}

		public void setPlayer( AxWindowsMediaPlayer _player )
		{
			m_video = _player;
		}

		public int getVolume()
		{
			return m_video.settings.volume;
		}

		public bool isValid()
		{
			return m_video != null;
		}

		public void Pause()
		{
			if (!isValid())
				return;

			m_video.Ctlcontrols.pause();
		}

		public bool isPlaying()
		{
			if (!isValid())
				return false;

			return m_video.playState == WMPLib.WMPPlayState.wmppsPlaying;
		}
		public void Play()
		{
			if (!isValid())
				return;

			m_video.Ctlcontrols.play();
		}
		public void SlideVideo(int _value)
		{
			if (!isValid())
				return;

			int maxTime = Convert.ToInt32(m_video.currentMedia.duration);

			int currentPos = maxTime * _value / 100;

			m_video.Ctlcontrols.currentPosition = currentPos;
		}

		public void SlideAudio(int _value)
		{
			if (!isValid())
				return;

			m_video.settings.volume = _value;
		}

		public void changeOwner(System.Windows.Forms.Control _owner)
		{
			if (!isValid())
				return;
		}

		public void stopVideo()
		{
			if (!isValid())
				return;

			m_video.Ctlcontrols.stop();
		}
	}
}
