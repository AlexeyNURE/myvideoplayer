﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoPlayer_v2.Model.Entity
{
	interface IPlaylist : IEnumerable<string>
	{
		string this[int index] { get; }

		string Name { get; }

		int getSize();

		void addVideoPath(string _path);

		void removeVideoPath(string _path);

		void removeAllPaths();

		bool hasPath(string _path);
	}
}
