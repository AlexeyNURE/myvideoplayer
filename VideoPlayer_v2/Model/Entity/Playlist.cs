﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoPlayer_v2.Model.Entity
{
	[Serializable]
	class Playlist : IPlaylist
	{
		//---------------------------------------------------------------------

		List<string> m_videoPaths;

		bool m_isPrivate;

		string m_password;

		string m_name { get; set; }

		//---------------------------------------------------------------------

		public string this[int index]
		{
			get => m_videoPaths[index];
		}

		public string Password
		{
			get => m_password;
			set
			{
				if (value == "")
					return;

				m_password = value;
				m_isPrivate = true;
			}
		}

		public string Name
		{
			get => m_name;
			set
			{
				if (string.IsNullOrEmpty(value))
					throw new ArgumentException("Invalid playlist name!");

				m_name = value;
			}
		}

		public bool isPrivate
		{
			get => m_isPrivate;
		}

		//---------------------------------------------------------------------

		public Playlist(string _name)
		{
			m_videoPaths = new List<string>();
			Name = _name;
			m_password = null;
			m_isPrivate = false;
		}

		//---------------------------------------------------------------------

		public void makePublic()
		{
			m_isPrivate = false;
			m_password = null;
		}

		public int getSize()
		{
			return m_videoPaths.Count;
		}

		public void addVideoPath( string _path)
		{
			m_videoPaths.Add(_path);
		}

		public void removeVideoPath(string _path)
		{
			if (!hasPath(_path))
				return;

			m_videoPaths.Remove(_path);
		}

		public void removeAllPaths()
		{
			m_videoPaths.Clear();
		}

		public bool hasPath(string _path)
		{
			return m_videoPaths.Contains(_path);
		}

		//---------------------------------------------------------------------

		public IEnumerator<string> GetEnumerator()
		{
			foreach (string path in m_videoPaths)
				yield return path;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			foreach (string path in m_videoPaths)
				yield return path;
		}
	}
}
