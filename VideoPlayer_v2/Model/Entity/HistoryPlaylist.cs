﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoPlayer_v2.Model.Entity
{
	[Serializable]
	class HistoryPlaylist : IPlaylist
	{
		//---------------------------------------------------------------------

		List<string> m_videoPaths;

		string m_name;

		//---------------------------------------------------------------------

		public string this[int index]
		{
			get => m_videoPaths[index];
		}

		public string Name
		{
			get => m_name;
		}

		//---------------------------------------------------------------------

		public HistoryPlaylist()
		{
			m_videoPaths = new List<string>();
			m_name = "History";
		}

		public int getSize()
		{
			return m_videoPaths.Count;
		}

		public void addVideoPath(string _path)
		{
			m_videoPaths.Add(_path);
		}

		public void removeVideoPath(string _path)
		{
			if (!hasPath(_path))
				return;

			m_videoPaths.Remove(_path);
		}

		public void removeAllPaths()
		{
			m_videoPaths.Clear();
		}

		public bool hasPath(string _path)
		{
			return m_videoPaths.Contains(_path);
		}

		//---------------------------------------------------------------------

		public IEnumerator<string> GetEnumerator()
		{
			foreach (string path in m_videoPaths)
				yield return path;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			foreach (string path in m_videoPaths)
				yield return path;
		}
	}
}
